// Setup colors:
let foreground = "black";
let midcolor = "lightgray";


// Initialize NVIS class
let nv = new myNvis("Nvis", 1);

// Setup On Change listener for all inputs
let inputs = document.getElementsByClassName("flex-select");
for (var i=0; i<inputs.length;i++) {
    inputs[i].addEventListener("change", () => selUpdate(nv))
}


// Get device orientation
let portrait = window.matchMedia("(orientation: portrait)").matches;


/*
 * Triggered on view change toggle
 * 
 * Sets correct div for block and calls for update
 */
function viewChange() {
    let sel = document.getElementById("selAct").value;
    let table = document.getElementById("plotTableDiv");
    let plotly = document.getElementById("plotly");

    table.style.display = "none";
    plotly.style.display = "none";
    if (sel == 1) {
        table.style.display = "block";
    } else if (sel == 9) { // Help
        document.getElementById("selAct").value = 1;
        window.location.href = "help.htm" 
    } else {
        plotly.style.display = "block";
    }
    selUpdate(nv)
}

// Trigger on page open
viewChange();

/*
 * Tooltip Toggle
 */
function toggleTooltip(hover) {
    document.getElementById("tooltip").style.display = hover?"block":"none";
}


/*
 * Called on any input change
 */
function selUpdate(nvis) {
    var sel, inx;
    sel=document.getElementById("selAct").value; 
    inx = parseInt(sel);
    nvis.viewMode=inx;
    sel=document.getElementById("state").value; 
    nvis.lat=parseFloat(sel);
    sel=document.getElementById("month").value;   
    nvis.month=parseInt(sel);
    sel=document.getElementById("year").value;  
    nvis.year=parseInt(sel);
    sel=document.getElementById("hiF2").value; 
    nvis.hF2=parseFloat(sel);  
    sel=document.getElementById("mast").value; 
    nvis.mast=parseFloat(sel);
    sel=document.getElementById("antenna").value; 
    nvis.antenna=parseInt(sel);
    sel=document.getElementById("mast2").value; 
    nvis.mast2=parseFloat(sel);
    sel=document.getElementById("antenna2").value; 
    nvis.antenna2=parseInt(sel);
    sel=document.getElementById("power").value; 
    nvis.power=parseInt(sel);
    sel=document.getElementById("elmin").value; 
    nvis.elevMin=parseInt(sel);
    sel=document.getElementById("dist").value;  
    nvis.distance=parseFloat(sel);
    sel=document.getElementById("loc").value;   
    nvis.location=parseInt(sel);
    sel=document.getElementById("storm").value; 
    nvis.storm=parseInt(sel);
    sel=document.getElementById("grx").value; 
    nvis.grxMode=parseInt(sel);
    console.log("selChange() ViewMode="+nvis.viewMode+",grxMode="+nvis.grxMode); 
    nvis.update();
    plotDraw(nvis);
}

/*
 * Select which plot type to calculate/display
 */
function plotDraw(nvis) {
    if(nvis.viewMode==1)  plotTable(nvis);
    if(nvis.viewMode==2)  plotSNR(nvis);
    if(nvis.viewMode==3)  plotSkip(nvis);
    if(nvis.viewMode==4)  plotSLM(nvis);
}

/*
 * Display Table
 */
function plotTable(nvis) {
	  console.log("Plotting table...")
	  console.log(nvis)
	  let table = document.getElementById("plotTable")
	  table.innerHTML = ""
	  let row = table.insertRow(0)
	  row.style.fontWeight = 600

    let titles = ["f", "Eirp", "Grx", "Fspl", "Drap", "Lt", "N", "SnrM", "SnrD", "SnrN"]
    for (let i=0;i<titles.length;i++) {
        row.insertCell(i).innerHTML = titles[i]
    }

	  for ( i=0; i<nvis.Eii.length; i++) {
        row = table.insertRow(i+1);
		    f = 1.5 + i*0.5; // Indexing 1 MHz 
        //if(i>8) { f = 8 + (i-11)*1; } // Don't do this
		    if (i>8 && i%2==0 ) {continue;} // Do this
		    row.insertCell(0).innerHTML = f  // f
		    if(f > nvis.muf3) { row.classList.add("warn") }; // Sets whole row
        if(f > nvis.muf3 * 1.18) { row.classList.add("alert") };  // Sets whole row
		    row.insertCell(1).innerHTML = Math.round(nvis.Eii[i]) // EIRP
		    row.insertCell(2).innerHTML = Math.round(nvis.Grx[i]) // GRX
		    row.insertCell(3).innerHTML = Math.round(nvis.Lii[i]) // FSLP
		    row.insertCell(4).innerHTML = Math.round(nvis.Ldd[i]) // DRAP
		    row.insertCell(5).innerHTML = Math.round(nvis.Ldd[i] + nvis.Lii[i]) // Total Loss (Lt)
		    row.insertCell(6).innerHTML = Math.round(nvis.Nnn[i]) // N
		    row.insertCell(7).innerHTML = Math.round(nvis.snrM[i]) // SNRM
		    row.insertCell(8).innerHTML = Math.round(nvis.snrD[i]) // SnrD
		    if(f > nvis.muf3*1.01) { row.cells[8].classList.add("alert") };
		    row.insertCell(9).innerHTML = Math.round(nvis.snrN[i]) // SnrN
		    if(f > nvis.muf1*1.18) { row.cells[9].classList.add("alert") };
    }  
	  document.getElementById("tooltip").innerHTML = `Eirp is transmitted signal power at origin in dBm units.
	  <br>Fspl is signal loss over signal path in dB units (daily maximum).
	  <br>Drap is signal loss in D layer in dB (daily maximum).
	  <br>Lt is total signal loss(Fspl+Drap) in dB (daily maximum).
	  <br>N is noise power at receive location in dBm units, for BW=3kHz.
	  <br>Snr is ratio of signal S and noise N in dB units.
	  <br>SnrM/D/N are Snr levels for midday/day/night.
	  <p style="color: red;">Signal must overcome noise, in order to be received.
	  <br>Minimum SNR is 10 dB for SSB voice, and 6 dB for data.</p>`
}

/*
 * Display Signal to Noise Ratio
 */
function plotSNR(nvis) {
	  console.log("Plotting SNR...")
	  var farr = [0]
	  var snrD = [0]
	  var snrM = [0]
	  var snrN = [0]
	  for (var i=0; i<nvis.Eii.length; i++) {
		    var f = 1.5 + i*0.5; // Indexing 1 MHz 
		    farr.push(f);
		    // Cap at 0
		    if (nvis.snrD[i] < 0) { snrD.push(0) } else { snrD.push(nvis.snrD[i]) }
		    if (nvis.snrM[i] < 0) { snrM.push(0) } else { snrM.push(nvis.snrM[i]) }
		    if (nvis.snrN[i] < 0) { snrN.push(0) } else { snrN.push(nvis.snrN[i]) }
    }  

	  var snrDSeries = {
		    x: farr, 
		    y: snrD, 
		    textfont: {family: 'Times New Roman'},
		    textposition: 'bottom center',
		    marker: {size: 12},
		    mode: 'lines',
		    type: 'scatter',
		    name: "Day",
        line: {color: '#29ff90',  width: 2}
	  };

	  var snrMSeries = {
        x: farr, 
        y: snrM, 
        textfont: {family: 'Times New Roman'},
        textposition: 'bottom center',
        marker: {size: 12},
        mode: 'lines',
        type: 'scatter',
        name: "Midday",
        line: {color: '#FF4c29',  width: 2}
	  };

	  var snrNSeries = {
		    x: farr, 
		    y: snrN, 
		    textfont: {family: 'Times New Roman'},
		    textposition: 'bottom center',
        marker: {size: 12},
		    mode: 'lines',
		    type: 'scatter',
		    name: "Night",
        line: {color: '#8229ff',  width: 2}
	  };

	  // Generic data for table
	  var layout = {
	      hovermode: 'x',
	      height: portrait?screen.height:document.querySelector('.plotsDiv').offsetHeight,
        autosize: true,
        font: {
            size: portrait?30:10,
            color: foreground
        },
        xaxis: {
            range: [ 0, 26 ],
            title: { text: "Db" },
            color: foreground,
            gridcolor: midcolor
		    },
        yaxis: {
            range: [0, 60],
            title: { text: "Div" },
            color: foreground,
            gridcolor: midcolor
        },
        legend: {
            x: portrait?0.8:1,
            y: portrait?1:0.5,
            yref: 'paper',
            font: {
                family: 'Arial, sans-serif',
                size: portrait?35:20,
                color: foreground
            },
        },
        title:'Signal to Noise Ratio 10 dB/div',
        paper_bgcolor: "#00000000",
        plot_bgcolor: "#00000000"

	  };
	  var config = {
		    displayModeBar: false,
		    responsive: true
	  }
	  var data = [ snrDSeries, snrMSeries, snrNSeries ];
	  Plotly.newPlot('plotly', data, layout,  config);
	  document.getElementById("tooltip").innerHTML = `Graph shows SNR levels for midday, day and night.
	      <br>SNR is ratio of signal S and noise N in decibel (dB) units.
	      <br>Minimum SNR is 10 dB for SSB voice, and 6 dB for data.
	      <br>Better antenna and more power can improve SNR.
	      <p style="color: red;">Using Frequency of Optimal Transmission (FOT) is the single most important factor.
	      <br>Ionsphere changes all the time and optimal frequencies also change.
	      <br>Critical frequencies raise during the day (say 10 MHz), and drop during the night (say 2 MHz).
	      <br>For daytime only link, we may need only one frequency.
	      <br>For day and night link, we will need at least two frequencies.</p>
	      <p style="color: green; text-align: left;">Some very common mistakes are:
	      <br>1. Using too high frequency for links under 500km.
	      <br>&nbsp;&nbsp;&nbsp;This creates a skip zone without coverage and can link only over 1000km and more.
	      <br>2. Using single frequency antenna (say AS-F104) for 3G.
	      <br>&nbsp;&nbsp;&nbsp;3G cannot find optimal frequency, it is stuck unless antenna is returned.
	      <br>&nbsp;&nbsp;&nbsp;Ionosphere keeps changing and link keeps fading in and out.</p>`
}

/*
 * Display Skip Zone
 */
function plotSkip(nvis) {
	  console.log("Plotting Skip...")
	  var skipX = []
	  var skipD = []
	  var skipM = []
	  var skipN = []

    // SkipM Calculation
    // Note: I'm not sure how 30 was originally chosen for x. Set to 1 for no particular reason.
    var x=1; var y=700; var fr=1.5; var started = false;
    for (var i=0; i<500; i++) {   // loop HF frequencies
        var skdi=0.0; var skslm=1.0;   // assume no skip zone
        var frrat = fr/nvis.fc3;    // ratio of frequency and critical foF2
        if(frrat > 1.05) {      // if over => we have skip
            for (var el=0; el<90; el++) { // loop over elevation angles
                skslm = nvis.skipSlm[el];
                if(skslm>frrat) skdi=nvis.skipDist[el];
            }
        }
        // Finding graph size limit
        if (skdi > 0) {
            started = true;
        }
        if (started && skdi <= 0) {
            var limit = i+20;
            for (var j=0;j<20;j++) {skipM.push(0);x+=5/30;skipX.push(x)}
            break;
        }

        if(skdi<0.0)  skdi=0.0;
        if(skdi>5000.0)  skdi=5000.0;
        // y=Math.round(525 - skdi*0.096);
        skipM.push(skdi)
        skipX.push(x)
        // No idea why 5/30
        x += 5/30;
        fr += 0.166667;
    }

    // SkipD Calculation
    x=1; y=700; fr=1.5;
    for ( i=0; i<limit; i++) {   // loop HF frequencies
        skdi=0.0; skslm=1.0;   // assume no skip zone
        frrat = fr/nvis.fc2;    // ratio of frequency and critical foF2
        if(frrat > 1.01) {      // if over => we have skip
            for (el=0; el<90; el++) { // loop over elevation angles
                skslm = nvis.skipSlm[el];
                if(skslm>frrat) skdi=nvis.skipDist[el];
            }
        }
        if(skdi<0.0)  skdi=0.0;
        if(skdi>5000.0)  skdi=5000.0;
        // y=Math.round(525 - skdi * 0.096);   // 5000 km into 480 pixels
        skipD.push(skdi)
        x+=5/30;
        fr+= 0.16666667;
    }

    // SkipN Calculation
    x=1; y=700; fr=1.5;
    for ( i=0; i<limit; i++) {   // loop HF frequencies
        skdi=0.0; skslm=1.0;   // assume no skip zone
        frrat = fr/nvis.fc1;    // ratio of frequency and critical foF2
        if(frrat > 1.05) {      // if over => we have skip
            for (el=0; el<90; el++) { // loop over elevation angles
                skslm = nvis.skipSlm[el];
                if(skslm>frrat) skdi=nvis.skipDist[el];
            }
        }
        if(skdi<0.0)  skdi=0.0;
        if(skdi>5000.0)  skdi=5000.0;
        skipN.push(skdi)
        x+=5/30;
        fr+= 0.1666667;
    }

    skipX = skipX.slice(0, limit)
    skipD = skipD.slice(0, limit)
    skipM = skipM.slice(0, limit)
    skipN = skipN.slice(0, limit)


	  var skipDSeries = {
		    x: skipX,
		    y: skipD,
		    textfont: {family: 'Times New Roman'},
		    textposition: 'bottom center',
		    marker: {size: 12},
		    mode: 'lines',
		    type: 'scatter',
		    name: "Day",
        line: {color: '#29ff90',  width: 2}
	  };

	  var skipMSeries = {
		    x: skipX, 
		    y: skipM, 
		    textfont: {family: 'Times New Roman'},
		    textposition: 'bottom center',
		    marker: {size: 12},
		    mode: 'lines',
		    type: 'scatter',
		    name: "Midday",
        line: {color: '#FF4c29',  width: 2}
	  };

	  var skipNSeries = {
		    x: skipX,
		    y: skipN,
		    textfont: {family: 'Times New Roman'},
		    textposition: 'bottom center',
		    marker: {size: 12},
		    mode: 'lines',
		    type: 'scatter',
		    name: "Night",
        line: {color: '#8229ff',  width: 2}
	  };
	  //
	  // Generic data for table
	  var layout = {
	      hovermode: 'x',
	      height: portrait?screen.height:document.querySelector('.plotsDiv').offsetHeight,
        autosize: true,
        font: {
            size: portrait?30:10,
            color: foreground
        },
        xaxis: {
			      // range: [ 0, 26 ],
			      title: { text: "MHz" },
            color: foreground,
            gridcolor: midcolor
        },
        yaxis: {
			      // range: [0, 60],
			      title: { text: "km" },
            color: foreground,
            gridcolor: midcolor
		    },
        legend: {
            x: portrait?0.8:1,
            y: portrait?0.1:0.5,
            yref: 'paper',
            font: {
                family: 'Arial, sans-serif',
                size: portrait?35:20,
                color: foreground
            }
        },
        title:'Skip Zone 500km/div',
        paper_bgcolor: "#00000000",
        plot_bgcolor: "#00000000"
	  };
	  var config = {
		    displayModeBar: false,
		    responsive: true
	  }
	  var data = [skipDSeries, skipMSeries, skipNSeries];
	  Plotly.newPlot('plotly', data, layout,  config);
	  document.getElementById("tooltip").innerHTML = `Graph shows skip zone for day, midday and night.<br>Skip zone is zone around transmiter without signal coverage.
    <br>Skip zone is big problem for NVIS use, but OK for SkyWave.</p>
    <p style="color: red;">If we use frequency below critical foF2, there will be no skip zone.
    <br>Using frequency over critical creates skip zone with no coverage.
    <br>Higher the frequency, larger the skip zone (up to 5,000 km).</p>
    Frequencies above 10 MHz are used for long distance (1,000 to 30,000 km).
    <br>Low frequencies are used for 0 to 500 km.
    <br>Good NVIS freqs are 2-4 MHz during night, and 4-8 MHz during day.`
}

/*
 * Display Secant Law Multiplier
 */
function plotSLM(nvis) {
	  console.log("Plotting SLM...")
	  var slmX = []
	  var SLM = []
	  var slmB = []

    // Calculate SLM
    var x=1;
    for (var el=0; el<90; el++) {   // loop elevation
        var skslm = nvis.skipSlm[el];
        if(skslm<1.0)  skslm=1.0;
        if(skslm>9.0)  skslm=9.0;
        // var y=Math.round(525 - skslm*96);
        SLM.push(skslm)
        slmX.push(x)
        x+=1;
    }

    // Calculate B
    var B;
    var x = 1;
    for (el=0; el<90; el++) {   // loop elevation
        B = nvis.skipB[el];
        if(B<0)  B=0;
        if(B>90.0)  B=90.0;
        slmB.push(B)
        x+=1;
    }

	  var slmSeries = {
		    x: slmX,
		    y: SLM,
		    textfont: {family: 'Times New Roman'},
		    textposition: 'bottom center',
		    marker: {size: 12},
		    mode: 'lines',
		    type: 'scatter',
		    name: "SLM",
        line: {color: '#29ff90',  width: 2},
		    yaxis: 'y2',
	  };

	  var slmBSeries = {
		    x: slmX,
		    y: slmB,
		    textfont: {family: 'Times New Roman'},
		    textposition: 'bottom center',
		    marker: {size: 12},
		    mode: 'lines',
		    type: 'scatter',
		    name: "B",
        line: {color: '#FF4c29',  width: 2}
	  };

	  // Generic data for table
	  var layout = {
	      hovermode: 'x',
	      height: portrait?screen.height:document.querySelector('.plotsDiv').offsetHeight,
        autosize: true,
        font: {
            size: portrait?30:10,
            color: foreground
        },
		    xaxis: {
			      // range: [ 0, 26 ],
			      title: { text: "EL\xB0" },
            color: foreground,
            gridcolor: midcolor
		    },
		    yaxis: {
			      range: [0, 100],
			      ticksuffix: "\xB0",
		            title: 'B',
            color: foreground,
            gridcolor: midcolor
		    },
		    yaxis2: {
		        range: [0, 5],
		            title: 'SLM',
		        overlaying: 'y',
            side: 'right',
            color: foreground,
            gridcolor: midcolor
		    },
        legend: {
            x: portrait?0.8:1,
            y: portrait?1:1,
            yref: 'paper',
            font: {
                family: 'Arial, sans-serif',
                size: portrait?35:20,
                color: foreground
            }
        },
        title:'Secant Law Multiplier',
        paper_bgcolor: "#00000000",
        plot_bgcolor: "#00000000"
	  };
	  var config = {
		    displayModeBar: false,
		    responsive: true
	  }
	  var data = [slmSeries, slmBSeries];
	  Plotly.newPlot('plotly', data, layout,  config);
	  document.getElementById("tooltip").innerHTML = `Critical frequency is the highest frequency Ionosphere will reflect back.
    <br>Frequencies above critical will pass through Ionosphere without reflection.
    <br>foF2 is critical frequency for layer F2 at vertical wave incidence.
    <br>F2 will reflect vertical wave if frequency is not over foF2.
    <br>Waves entering F2 layer at lower angles will have higher critical frequency fc.
    <br>red
    <br>This relationship is called Secant Law => fc = foF2 * sec (B).
    <br>Wave is sent at elevation angle EL (horizon is 0, up is 90 degrees).
    <br>Wave enters F2 layer at angle B (perpendicular is 90 degrees).
    <br>blue
    <br>Secant Law Multiplier SLM increases critical frequency by factor of 1 to 6.
    <br>For wave entering F2 at 30 degrees critical frequency fc = 2 * foF2.`
}



/*
 * Dark Mode
 */
const checkbox = document.getElementById("checkbox");
// Set default to light
checkbox.checked = false;

checkbox.addEventListener('change', () => {
    if (checkbox.checked) {
        document.body.classList.add("dark");
        foreground = "lightgray";
        midcolor = "#405362";
    } else {
        document.body.classList.remove("dark");
        foreground = "black";
        midcolor = "lightgray";
    }
    selUpdate(nv);
    // document.getElementsByClassName("myDiv")[0].classList.toggle("dark");
})
