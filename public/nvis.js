class myNvis {
    constructor(name, c) {
        console.log("nvisContructor");
        this.id=name;       
        this.code=c; 
        var dt = new Date(); // Current date 
        this.year = dt.getFullYear();   // TD
        this.month = dt.getMonth()+1;  // TD why this?
        this.lat=-38;       this.lon=-144;    // TD Why this?
        this.ssn=80; // TD Why any of this?!
        this.viewMode=1;    this.distance=100;  
        this.gain=6;        this.power=52;      this.hops=1; 
        this.location=0;    this.storm=0;     this.eirp = 64;  
        this.hF2 = 300.0;   this.elev=90;     this.elevMin=10; this.freq=2.2;
        this.mast=12;       this.antenna=1;             //dipole at 12 m
        this.cycleCoe=1.0;  this.seasonCoe=1.0; this.latCoe=1.0; // Correction factors 
        this.fc1=2.3;  this.fc2=4.0;  this.fc3=5.0;     // foF2 (night, day, noon)
        this.muf1=2.5; this.muf2=4.1; this.muf3=5.1;    // MUF (night, day, noon)
        this.slm=1.0;  this.pathdist=100.0;  this.B=0.2; // secant law multiplier
        this.tria=6731; this.trib=7031; this.tric=300;  // great circle trianlge sides
        this.triAA=1;   this.triBB=1; this.triCC=1;     // great circle triangle angles
        this.maxHop = 3000.0;
        this.grxMode = 0;
        this.snrM = Array(62).fill(-30);    
        this.snrD = Array(62).fill(-30); 
        this.snrN = Array(62).fill(-30);
        this.Lii  = Array(62).fill(200);    
        this.Ldd  = Array(62).fill(200);
        this.Eii  = Array(62).fill(47);     
        this.Nnn  = Array(62).fill(-130);
        this.Gtx  = Array(62).fill(-40);
        this.Grx  = Array(62).fill(-40);
        this.skipDist = Array(92).fill(0);
        this.skipSlm =  Array(92).fill(1);
        this.skipB =  Array(92).fill(0);
        console.log("this() id="+this.id+",code="+this.code+", len=" + this.skipSlm.length);
    }

    update() {
        console.log("nvisPredict(1)");
        this.calcMaxHop();       // max hop distance
        this.calcHops();     // calc hops and elevation, calls Dist2El()
        this.calcCoe();      // calc cycleCoe, latCoe and seasonCoe
        this.calcfoF2();     // estimate foF2 
        this.latestfoF2();   // latest Ionosonde data mixed with estimate
        this.calcMuf();      // calc MUF from foF2 (add half gyro, mult with SLM)
        this.caclSkip();     // calc skip zone, SLM and angle B
        this.calcSNR();      // calc SNR for night, day and midday
    }

    calcMaxHop() {      // Maximum hop distance on great circle
        var A, B,C;                // Triangle angles
        var a, b = 6371, c;        // Triangle sides, b=earth radius
        c= b + this.hF2;           // sides b and c are known 
        C = D2R(90 + 0);           // C = 90deg + elev, max hop is for elev=0 
        a = Math.sqrt(c*c - b*b);  // Pythagoras
        A = Math.asin(a/c);        // sina A = a/c
        var d = 40000.0 * A / Math.PI ; // great circle perimeter is 40,000 km
        d -= 50;
        console.log("maxHop(" + this.hF2 + ")="+ d.toFixed(1));
        this.maxHop = d;
    }

    D2E() {      // Distance to elevation on great circle
        var d, e;
        var s = "D2E(" + this.distance.toFixed(0) + ") ";
        if(this.distance > this.maxHop) { 
            this.elev=-1.0;
            this.pathDistance=5000.0;
            console.log(s + " Error  d=", this.distance.toFixed(1));
            return -1;
        }
        var A, B,C;                   // Triangle angles
        var a, b = 6371, c;           // Triangle sides 
        c = b + this.hF2;              // sides b and c are known constants
        A = Math.PI * this.distance / 40000;      // calc A from great circle angular distance
        a = (c*c) + (b*b) - 2 * (b*c) * Math.cos(A);    // cos rule
        a = Math.sqrt(a);                         // side a = path.dist/2
        console.log(s + ") A=" + R2D(A).toFixed(2) + ",  a=" + a.toFixed(1));
        B = (b / a) * Math.sin(A); 
        B = Math.asin(B);                           // sine law => B
        C = Math.PI - A - B;                        // C = 180-A-B
        console.log(s+ "B:" + R2D(B).toFixed(2) + ", C=" + R2D(C).toFixed(C));
        this.elev = C - Math.PI / 2;                // Elev = C-90deg
        this.slm = 1.0 / Math.cos(B);               // secant law multiplier
        this.pathdist = a * 2;                      // FSPL distance
        this.B = B;
        console.log(s+ "El=" + R2D(this.elev).toFixed(1) + ", PaDi=" + this.pathdist.toFixed(1)+", slm=" + this.slm.toFixed(3)); 
        return this.elev; // TD this doesnt make sence. Function should either return or set variables. NOT BOTH.
    }

    calcHops() {   // No of hops, limited by minimum site elevation
        var el1, el2, di1, hops=1;
        el1 = D2R(this.elevMin);   // site minimum elevation - deg to rad
        di1 = this.distance;
        el2 = this.D2E();      // elevation angle in radians
        while (el2 < el1) {
            hops++;
            this.distance = di1 / hops;
            el2 = this.D2E(); // elevation angle in radians
        }
        console.log("calcHops(1) hops="+hops+", el1="+ R2D(el1).toFixed(1)+", el2="+R2D(el2).toFixed(1));
        this.elev = el2;  
        this.hops = hops; 
        this.elev *= 180 / 3.1414;   // into degrees
    }

    calcCoe() { // Current SSN, cycleCoe, seasonCoe, latCoe
        var yr = (12*this.year+ this.month)/12;
        this.cycleCoe = 1.0 - (Math.abs(2025.5 - yr)) / 6.0; 
        this.ssn = this.cycleCoe * 170;
        //console.log("cycleCor() Yr=" + this.year + ",cycleCoe=" + this.cycleCoe);
        this.seasonCoe = (Math.abs(this.month - 6.0)) / 6.0;
        //console.log("cycleCor() Mo=" + this.month + ",seasonCoe=" + this.seasonCoe);
        this.latCoe = (this.lat + 43) / 31;
        console.log("cycleCor() lat=" + this.lat.toFixed(3) + ", latCoe=" + this.latCoe.toFixed(3));
    } 

    calcfoF2() {  // foF2 daily minimum   min 2.0, lat+0.5, fold at S 23 
        var c, d, e, f;
        c = this.latCoe;  d = this.seasonCoe; e = this.cycleCoe;  
        // find minimum foF2
        f = 1.8 + (d * 0.8);     // winter 1.8, summer 2.6
        f *= (e + 1);            // Solar peak doubles
        if(f < 1.8) { f = 1.8;}    // Low limit
        if(f > 6.5) { f = 6.5;}    // hi limit
        this.fc1=f;
        // find maximum foF2 
        c=this.latCoe;  d = this.seasonCoe; e = this.cycleCoe;
        f = 4.7 + c;             // low season first 4.7 to 5.7
        if(d > 0.5  &&  c < 0.65 ) { d = 0.5 }  // summer and equinox equal
        f *= 1 + 0.9*d;   // summer almost doubles in tropics
        f *= (1 + 1.8*e);   // half cycle is 10% improvement
        if(f < 4.7)  { f = 4.7; } 
        if(f > 14.3) { f = 14.3;}
        this.fc3 = f;                   // daily maximum
        this.fc2 = (f + this.fc1) / 2;    // mid value
        console.log("caclfoF2() fc1=" + this.fc1.toFixed(1) + ", fc3=" + this.fc3.toFixed(1));
    } 

    latestfoF2() {  // current foF2 min max from Ionosondes
        var t=this.lat;
        var f1=3.0, f3=8.0;          // Mawson Station, Antarctica   
        if(t>-50) {f1=2.3; f3=8.0; } // Hobart
        if(t>-40) {f1=2.8; f3=9.3; } // Learmonth, Vic
        if(t>-36) {f1=2.8; f3=8.1; } // Canberra
        if(t>-34.5) {f1=2.8; f3=8.3; } // Camden, Sydney
        if(t>-32.5) {f1=2.9; f3=8.3; } // Perth
        if(t>-31) {f1=3.0; f3=8.5; } // Brisbane
        if(t>-23) {f1=2.7; f3=9.0; } // Townsville
        if(t>-15) {f1=2.4; f3=9.5; } // Darwin
        let f2 = (f1+f3)/2;// adjust f2
        // Mix with prediction
        var ye=2022, mo=6, da=12;   // date when Ionosonde adjusted  
        var d1 = ye*365 + mo*30.5 + da;
        var d2 = this.year*365 + this.month*30.5 + 15; // adjustment age in days
        var me=(d2-d1)/90; me=Math.abs(me); // mix factor
        if(me > 1.0) me=1.0;
        this.fc1*=me; this.fc1+=f1*(1-me);
        this.fc2*=me; this.fc2+=f2*(1-me);
        this.fc3*=me; this.fc3+=f3*(1-me);
        console.log("latestfoF2(), f1= " + f1.toFixed(1) + ",f3=" + f3.toFixed(1) + ",me=" + me.toFixed(2));
    }  

    calcMuf() {   // Maximum Usable Frequencies (MUF)
        var c = this.fc2;
        var fh=0.6;
        if(this.distance > 150)   fh = 0.45;  // half gyro frequency in MHz
        if(this.distance > 600)   fh = 0.32;
        if(this.distance > 1200)  fh = 0.23;
        if(this.distance > 2500)  fh = 0.15;
        this.muf1 = this.fc1 + fh;            // add half gyro
        this.muf1 *= 0.9 * this.slm;          // multiply with SLM
        this.muf2 = this.fc2 + fh;  
        this.muf2 *= 0.9 * this.slm;
        this.muf3 = this.fc3 + fh;  
        this.muf3 *= 0.9 * this.slm;
        console.log("calcMuf(), f1= " + this.muf1.toFixed(1) + ",muf3=" + this.muf3.toFixed(1));
    }

    showSel() {
        var s= "Lat=" + this.lat  + ", Mon=" + this.month + ", Yr=" + this.year;
        return s;
    }

    showCoe() {
        var s;
        s = "showCoe() Cyc=" + this.cycleCoe.toFixed(2); 
        s += ", Sea=" + this.seasonCoe.toFixed(2); 
        s += ", Lat=" + this.latCoe.toFixed(2); 
        return s;
    }

    showfoF2() {
        var s;
        s = "foF2: "+this.fc1.toFixed(1)+", "+this.fc2.toFixed(1);
        s += ", "+this.fc3.toFixed(1);
        s += ", SSN:" + this.ssn.toFixed(0)+", SLM:" + this.slm.toFixed(2);
        return s;
    }

    showMuf() {
        var s1 = "FOT: " + this.muf1.toFixed(1);
        s1 += ", " + this.muf2.toFixed(1);
        s1 += ", " + this.muf3.toFixed(1);
        s1 += ", Hop:" + this.hops;
        var s2 = '\xB0';
        s1 += ", El:" + this.elev.toFixed(1) + s2;
        var c = R2D(this.B);
        s1 += ", B:" + c.toFixed(1) + s2;
        return s1;  
    }

    calcSNR() {    // frequency scan
        console.log("calcSNR() started, PaDi=" + this.pathdist.toFixed(1));
        var i, li, ld, n, mf;
        this.freq = 1.5;
        for( i=0; i<58; i++) {   // Calculate SNR data
            this.snrM[i]=-30.0; 
            this.snrD[i]=-30.0; 
            this.snrN[i]=-30.0;
            this.antennaGain();  // Gt in dBm
            this.Gtx[i]=this.gain;
            this.Grx[i]=this.gain2;
            this.eirp= this.power + this.gain; // Eirp in dBm
            this.Eii[i]=this.eirp;
            li= calcFSPL(this); 
            this.Lii[i] = li; 
            ld= calcDrap(this);  
            this.Ldd[i] = ld;
            n = calcNoise(this);  
            this.Nnn[i] = n;   
            //this.snrM[i] = this.eirp + this.gain2 - li - ld - n; // MidDay Snr
            this.snrM[i] = this.eirp - li - ld - n + this.gain2; // MidDay Snr
            mf= this.muf3*1.2;
            if(this.freq > mf) this.snrM[i] = -30.0;
            this.snrD[i] = this.snrM[i] + 0.3333 * ld;       // Day SNR
            mf = this.muf2 * 1.2; 
            if(this.freq > mf) this.snrD[i] = -30.0;
            this.snrN[i] = this.snrM[i] + ld - 10;      // Night SNR
            mf = this.muf1 * 1.2; 
            if(this.freq > mf) this.snrN[i] = -30.0;
            mf = this.freq / this.fc3;                    // Midday frequency radio
            if(i==0 || i==10) console.log("calcSNR("+this.freq.toFixed(2)+") FSPL="+this.Lii[i].toFixed(0)); 
            this.freq += 0.5
        } 
    }

    // Use hF2 and loop elevation angle C => calculate SLM and skip distance
    caclSkip() {    // save skip and slm for each degree elev (0 to 90)
        var el, v1, v2, di;        
        var a, b=6371, c, A, B,C;       // Triangle with b = Earth radius 
        c= b + this.hF2;                // triangle side c = b+hF2
        // sides b and c are known , angle C will loop
        for(el=0; el<91; el++) {        // wave elevation angle (horizon=0)
            C = 90 + el;                  // triangle angle C for given el
            C  =D2R(C);                   // angle C in radians
            v1 = (b / c) * Math.sin(C);   // sinus rule: b/c = sinB/sinC => sinB= (b/c) *sinC
            B = Math.asin(v1);            // wave incidence angle into F2 (vertical=normal=0)
            v2 = 1.0 / Math.cos(B);       // secant law multiplier (SLM)
            this.skipSlm[el] = v2;        // store SLM into array, 1 for every elevation
            this.skipB[el] = R2D(B);      // store angle B in degrees (incident angle into F2 layer)
            A = Math.PI - C - B;          // angle A from A+B+C=180 degrees
            di= 40000.0 * A / Math.PI;    // terrestrial distance as arc of great circle
            this.skipDist[el]= di;        // save skip distance into array
            if(el==0)  console.log("calcSkip(" + el + ") Di=" + di.toFixed(0) + " slm=" + v2.toFixed(3)+",B="+R2D(B).toFixed(1));
        }
    }

    antennaGain() {      // Gets Gtx and Grx and updates nvis class var s1, s2, s3;                 // declare few strings var g=[-20.0,-20];              // initialise gain array
        var fr=this.freq, a=this.antenna,  h=this.mast, e=this.elev;  // ant parameters
        let s1="antennaGain() ";            // needed for console debug
        let s2=fr.toFixed(1) + " e="+e.toFixed(1) + ", h=" + h.toFixed(1); // more console debug
        // receive antenna gain Grx
        a=this.antenna2; h=this.mast2;   
        // transmit antenna gain  g=[G,Gavg]
        let g = myAntennaGain(a, fr, h, e);  
        this.gain=g[0];                  // update class gain= Gtx
        // update this class gain2= Grx
        this.gain2=0;                                 // case Grx off              
        if(this.grxMode == 1) this.gain2=g[0];        // case Grx on     
        if(this.grxMode == 2) this.gain2=g[0]-g[1];   // case Grx-Avg
        // console debug
        let s3=" Gtx=" + this.gain.toFixed(1) + ",Grx=" + this.gain2.toFixed(1);  
        //console.log(s1 + "Rx " + s2 + s3); 
        console.log(s1 + s3) ;
    }
}


function D2R (n) { var n2 = Math.PI / 180; return n*n2;}
function R2D (n) { var n2 = Math.PI / 180; return n/n2;}







